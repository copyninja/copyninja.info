About
#####

:date: 2014-02-01 23:16
:author: Vasudeva Kamath

I'm Vasudeva Kamath pronounced as *Vāsudēva Kāmat* , a Software
Engineer from India. I hold a dayjob at a MNC in day and night time
hack on free software project which interests me.

I grew up and studied in city of `Karkala
<http://en.wikipedia.org/wiki/Karkala>`_ which is in State of
Karnataka in South India (West Coast). Currently I live in Bangalore
where I do my day job.

Free and Open Source software has been my area of interest from long
time and I've been contributing to various FLOSS project in my free
time, of which most notable is `Debian Project
<http://www.debian.org>`_.

At work I code mainly in *C* and as hobby I code in *Python*. I also
know languages like *Java, PHP, a bit of Perl, Go* syntactically. What
I mean by syntactically is that give any projects in these languages
and the problem statement I can hack on it and solve the problems.

I use a lot of *Shell scripting* and GNU Make in both official and
FOSS work. Git is my choice for VCS, I do know bit of Mercurial
too. I've worked with SVN also. I use Apache as web browser, started
using Nginx these days. I use uWSGI as application server for Python
and PHP services. Emacs is my choice for editor and *openbsd-cwm* is
my choice of desktop (window manager).

Besides coding I also hold Vidwat degree in `Hindustani Tabla
<http://en.wikipedia.org/wiki/Tabla>`_ and I've been teaching it for 5
years (not at the moment though).

Finally this site doesn't track you using any sort of analytics nor
provides any way to comment if you have comments consider sending me
it over mail, for the details check Contacts page.
